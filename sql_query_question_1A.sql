SELECT "Pessoas"."ds_nome",
    "Cargos"."ds_cargo"
FROM "Pessoas"
    INNER JOIN "Cargos" ON ("Pessoas"."id_cargo" = "Cargos"."id")
ORDER BY "Pessoas"."dt_admissao";