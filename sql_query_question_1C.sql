SELECT "Cargos"."ds_cargo",
    COUNT("Pessoas"."id") AS "quantity_employees"
FROM "Cargos"
    LEFT OUTER JOIN "Pessoas" ON ("Cargos"."id" = "Pessoas"."id_cargo")
GROUP BY "Cargos"."id";