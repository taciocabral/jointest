from django.db import models


class Cargos(models.Model):
    nome_cargo = models.CharField(max_length=100, db_column="ds_cargo")

    class Meta:
        db_table = "Cargos"


class Pessoas(models.Model):
    nome = models.CharField(max_length=150, db_column="ds_nome")
    cargo = models.ForeignKey(
        Cargos,
        on_delete=models.PROTECT,
        related_name="pessoas_cargos",
        related_query_name="pessoa_cargo",
        db_column="id_cargo",
    )
    admissao = models.DateField(db_column="dt_admissao")

    class Meta:
        db_table = "Pessoas"

