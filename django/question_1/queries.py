from django.db.models import Count

from question_1.models import *


def employees_and_positions_in_order_of_admission():

    return list(
        Pessoas.objects.select_related("cargo")
        .only("nome", "cargo__nome_cargo")
        .order_by("admissao")
        .values_list("nome", "cargo__nome_cargo")
    )


def oldest_employee():

    return (
        Pessoas.objects.select_related("cargo")
        .only("nome", "cargo__nome_cargo")
        .values_list("nome", "cargo__nome_cargo")
        .earliest("admissao")
    )


def positions_employess_quantity():

    return list(
        Cargos.objects.annotate(quantity_employees=Count("pessoa_cargo")).values_list(
            "nome_cargo", "quantity_employees"
        )
    )
