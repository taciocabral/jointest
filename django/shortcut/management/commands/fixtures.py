import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Install fixtures to populate database"

    def add_arguments(self, parser):
        # parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        f = open("fixtures.txt", "r")
        fixtures_names = f.readlines()
        f.close()
        for n in fixtures_names:
            os.system(f"python manage.py loaddata {n}")
        print("\n Success! \n")
