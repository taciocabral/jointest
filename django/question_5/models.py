from django.db import models


class Admin(models.Model):
    name = models.CharField(max_length=10)


class Address(models.Model):
    street = models.CharField(max_length=100)
    number = models.IntegerField()


class User(models.Model):
    username = models.CharField(max_length=10)
    addres = models.OneToOneField(Address, on_delete=models.PROTECT,)


class Group(models.Model):
    admin = models.ForeignKey(Admin, on_delete=models.PROTECT)
    users = models.ManyToManyField(User)
